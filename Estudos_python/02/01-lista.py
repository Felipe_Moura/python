minha_lista1 = ["abacaxi", "melancia", "abacate"]
minha_lista2 = [1, 2, 3, 4, 5]
minha_lista3 = ["abacaxi", 2, 9.89, True]

print(minha_lista1)

for item in minha_lista2:
    print(item)

minha_lista1.append("limao") # adiciona um elemento a lista

print(minha_lista1)

if 3 in minha_lista2:
    print("3 está na lista")

del minha_lista2[2:]
print(minha_lista2)
