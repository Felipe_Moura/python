# media, mediana, moda
#from statistics import *


#def mediana(lista):
#median(lista)
#return media


#mean(lista)
#median(lista)   #usando bibliotecas nativas do python
#mode(lista)
#return media

from statistics import *

def media (lista):
    return mean(lista)

def mediana (lista):
    return median(lista)

def moda (lista):
    return mode(lista)